@extends('layouts.app')

@section('content')
<div class="az-header">
    <div class="container">
        <div class="az-header-left">
            <h2 class="az-content-title tx-24 mg-b-25">Grafik Mapping Talent</h2>
        </div>
    </div>
</div>
<body class="az-body">
    
    <div class="az-content az-content-app pd-b-0">
        <div class="container">
            <div class="az-content-body">
                <div style="width: 800px;margin: 0px auto;">
                    <canvas id="myChart"></canvas>
                </div>
            </div>
        </div>
    </div>

<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>
<script src="{{asset('js/azia.js')}}"></script>
<script src="{{asset('js/chart.js')}}"></script>
<script>
    $(document).ready(function () {

        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'scatter',
            data: {
                datasets: [{
                    data: [
                        @foreach($grafik as $gfk)
                        {
                            x:{{ $gfk->kinerja }},
                            y:{{ $gfk->potensi }}
                        },
                        @endforeach
                    ],

                    backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)'
                    ],

                    borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            suggestedMin: 0,
                            suggestedMax: 3,
                            stepSize: 1
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero:true,
                            suggestedMin: 0,
                            suggestedMax: 3,
                            stepSize: 1
                        }
                    }]
                }
            }
        });

    });
</script>
</body>
@endsection
